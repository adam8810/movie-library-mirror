$('document').ready(function()
{
    $user_id = $('#user_id').attr('value');

    $('#movie_info').on('shown', function() {
        $('.nav-tabs li').each(function() {
            //                this.removeClass('active')
        });
        //            $('.nav-tabs li').first().attr('class','active');
    });

    $('#search_toggle_btn_group a, .cast_btn, #modal_release_date').tooltip({
        delay: {
            show: 400,
            hide: 10
        }
    });

    $add_movie_url = '/api/add_to_list.json';


    $('.modal').on('shown', function() {
        $('body').css('overflow', 'hidden');
    });

    $('.modal').on('hidden', function() {
        $('body').css('overflow', 'auto');
        $('select#add_to_list').empty();
    });

    $('.thumbnail_button').click(function() {

        $('#modal_backdrop').css('display', 'none');
        $('.modal-footer, #modal_content, .nav-tabs, button.close, #modal_rating').css('display', 'none');
        $('#modal_progress').css('display', 'block');
        $('#modal_loading').css('display', 'block');
        $('.rating_star').css('display', 'none');


        $('#modal_additional_info').text("");
        $('#modal_title').text("");
        $('#modal_trailers').text("");
        $('#modal_tagline').text("");
        $('#modal_cast').text("");
        $('#modal_desc').text("");
        $('#modal_backdrop').text("");
        $('#modal_runtime').text("");
        $('#modal_collection').text("");
        $('#modal_rating').text("");

        $('#modal_title').attr('title', this.id);

        $id = $(this).attr('id');

        $.ajax({
            url: '/api/movie.json?id=' + $id,
            success: function(data)
            {
                $('#modal_title').text(data.title)
                $('#modal_release_date').text(" (" + data.release_date_year + ")")
                $('#modal_release_date').attr('title', data.release_date_full)
                $('#modal_desc').text(data.overview)
                if (data.tagline != "")
                    $('#modal_tagline').text(data.tagline)
                $('#modal_runtime').text("Runtime: " + Math.floor(data.runtime / 60) + " hours " + data.runtime % 60 + " minutes")

                if (data.collection != null)
                    $('#modal_additional_info').append("<p>" + data.collection + "</p>");

                if (data.trailer != null)
                {
                    $('#modal_trailers').append('<li class="thumbnail"><iframe src="http://www.youtube.com/embed/' + data.trailer + '" frameborder="0" allowfullscreen></iframe></li>')
                }

                for ($i = 0; $i < data.vote_average; $i++)
                {
                    $('#' + ($i + 1) + 'star').css('display', 'block');
                }

                if (data.mpaa_rating != null)
                {
                    $('#modal_rating').removeClass('badge badge-info badge-success badge-warning badge-important').css('display', 'block').text(data.mpaa_rating);

                    switch (data.mpaa_rating)
                    {
                        case 'G':
                            $('#modal_rating').addClass('badge badge-success')
                            break;

                        case 'PG':
                            $('#modal_rating').addClass('badge badge-info')
                            break;
                        case 'PG-13':
                            $('#modal_rating').addClass('badge badge-warning')
                            break;
                        case 'R':
                            $('#modal_rating').addClass('badge badge-important')
                            break;
                        case 'NR':
                            $('#modal_rating').addClass('badge')
                            break;
                    }
                }
                else
                    $('#modal_rating').removeClass('badge badge-info badge-success badge-warning badge-important');

                $.ajax({
                    url: '/api/cast.json?id=' + $id,
                    dataType: 'json',
                    success: function(data)
                    {
                        $(data.cast).each(function($index)
                        {
                            // Set defaults
                            $profile_path = "/assets/img/missing_" + (($index % 5) + 1) + ".png";
                            $position_offsets = "";

                            // Check to see if image exists
                            if (this.profile_path != null)
                            {
                                $profile_path = "http://cf2.imgobject.com/t/p/w185" + this.profile_path;
                                $position_offsets = "-20px -40px";
                            }

                            $('#modal_cast').append(
                                    '<li title="' + this.character + '" class="cast_btn">'
                                    + '<a href="http://www.themoviedb.org/person/' + this.id + '" style="display:block" class="btn thumbnail">'
                                    + '<div class="cast_li img-rounded" style="background: url(' + $profile_path + ')' + $position_offsets + '">' + '</div>' +
                                    '<p style="text-align: center; margin:5px 0 0 0">' + this.name + '</p>' + '</a>' + '</li>');
                        });
                    }
                });
                $.ajax({
                    url: '/api/user_lists.json',
                    type: 'GET',
                    dataType: 'json',
//                    async: false,
                    data: {
                        user_id: $user_id,
                        movie_id: $id
                    },
                    success: function(data) {
                    var selected = data.selected;
                        $('#add_to_list').select2({
                            data: data.lists,
                            multiple: true,
                            width: '99%',
			    placeholder: 'Add to List',
                            initSelection: function(element, callback) {
                                console.log(selected);
                                var data = [];
                                $(selected).each(function() {
//                                    console.log(selected[this]);
                                    data.push({id: this.id, text: this.text });
                                });
                                callback(data);
                            }
                        }).select2("val", [1,2]);
//                        console.log(data.selected);

                        $('#add_to_list').on('change', function($e) {
                            console.log($e);
                            if ($e.added !== undefined) {
//                                console.log($e.added);
                                $.ajax({
                                    url: '/api/add_to_list.json',
                                    data: {list_id: $e.added.id, movie_id: $id, user_id: $user_id},
                                    success: function() {
                                    }
                                });
                            }

                            if ($e.removed !== undefined) {
//                                console.log($e.removed)
                                $.ajax({
                                    url: '/api/remove_from_list.json',
                                    data: {list_id: $e.removed.id, movie_id: $id},
                                    success: function() {
                                    }
                                });
                            }
                        });

                        $('select#add_to_list').show();
                    }
                });

                $('select#add_to_list').change(function() {
                    $(this.options + ':selected').each(function() {

                    });
                });


                if (data.backdrop !== "")
                {
                    $('#modal_backdrop').css('background', 'url(http://cf2.imgobject.com/t/p/w780/' + data.backdrop + ') no-repeat -120px -55px');
                }
                else
                    $('#modal_backdrop').css("background", 'url(/assets/img/missing_backdrop_' + Math.ceil(Math.random() * 5) + '.png) 0 0px');

                $('#modal_progress').css('display', 'none');
                $('#modal_backdrop').css('display', 'block');
                $('.modal-footer, #modal_content, .nav-tabs, button.close').css('display', 'block');
                $('#modal_loading').css('display', 'none');
            },
            dataType: 'json'
        });
    });

    if (0) {
        var $hash = window.location.hash.substring();

        $.get('get_view', function(view) {

            if (window.location.hash)
            {
                switch ($hash)
                {
                    case '#list':
                        $.ajax({
                            url: 'set_view/list'
                        });
                        view_list();

                        break;
                    case '#albumlist':
                        $.ajax({
                            url: 'set_view/albumlist'
                        });
                        view_albumlist();

                        break;
                    case '#grid':
                        $.ajax({
                            url: 'set_view/grid'
                        });
                        view_grid();
                        break;
                }
            }
            else if (view != '') // If the view session variable isn't set, default to albumlist()'
            {
                switch (view[0])
                {
                    case 'l':
                        view_list();

                        break;
                    case 'a':
                        view_albumlist();

                        break;
                    case 'g':
                        view_grid();
                        break;
                }
            }
            else
            {
                view_albumlist();
            }
        });

        $('.movie_head_item.content.' + $orderby + ' a').css('background', '#D6DAE0');

        // Hover effect of Add/Remove Button
        $('.img_wrapper').hover(function() { // Hover on
            var value_id = this.id;

            //            setTimeout(function() {
            $('#' + value_id + ' .movie_info').slideDown('fast').css('display', 'block');
            $('#' + value_id + ' .movie_btn').slideDown(200).css('display', 'block');
            //            }, 300, value_id);

        }, function() { // Off
            $('#' + this.id + ' .movie_info').slideUp(90)
            $('#' + this.id + ' .movie_btn').slideUp(190)

        })

        $('.transition_out').click(function(e)
        {
            $('.movie_item#' + this.id).animate({
                width: 0,
                height: 0
            }, 300, "linear", function() {
                $('.movie_item#' + this.id).css('display', 'none')
            });
        });

        $('a.view_btn').click(function(e) {
            e.preventDefault();

            switch (this.id)
            {
                case 'list':
                    $.ajax({
                        url: 'set_view/list'
                    });
                    view_list();
                    break;

                case 'albumlist':
                    $.ajax({
                        url: 'set_view/albumlist'
                    });
                    view_albumlist();

                    break;
                case 'grid':
                    $.ajax({
                        url: 'set_view/grid'
                    });
                    view_grid();
                    break;
            }
        });

        $('a.background_link').click(function(e)
        {
            e.preventDefault();

            $.ajax({
                url: this,
                success: function() {

                }
            });
        });

        // Stars
        $('.5star').hover(function() {
            $('a#' + this.id + '.background_link.1star').css('display', 'none');
            $('a#' + this.id + '.1star').addClass('full_star');
        }, function() {
            $('#1star').removeClass('full_star');
        })


        function view_list()
        {
            $('div#movie_info_box').css('display', 'block');
            $('span.header').css('display', 'none');
            $('div.movie_item').css('padding', '5px 5px 5px 50px');

            $('div#movie_head').css('display', 'block');
            $('.movie_item').css('height', '50px').css('width', '100%').css('margin-bottom', '0px');

            $('div.content').css('float', 'left');
            $('div#movie_head').css('display', 'block');
            $('div.movie_item.odd').css('background', '#F3F6FA');


            $('.img_wrapper').css('height', '50px').css('width', '40px');
            $('.movie_head_item.image').css('width', '40px').css('height', '37px').css('background', '#F3F6FA').css('text-indent', '-9999');


            $('.content.title').css('width', '50%').css('display', 'inline');
            $('.content.runtime').css('width', '10%').css('text-align', 'right').css('display', 'block');
            $('.content.year').css('width', '10%').css('text-align', 'right').css('display', 'block');
            $('.content.m_rating').css('width', '10%').css('text-align', 'right').css('display', 'block');
            $('.content.our_rating').css('width', '10%').css('text-align', 'right').css('display', 'block');
        }

        function view_albumlist()
        {
            $('div#movie_info_box').css('display', 'block');
            $('.img_wrapper').css('display', 'block').css('height', '200px').css('width', '145px');
            $('span.header').css('display', 'inline');
            $('div.movie_item').css('padding', '5px 10px 10px 150px').css('float', 'none').css('width', '100%').css('height', '187px');
            $('.content').css('width', '100%');
            $('div.content').css('display', 'block');
            $('div#movie_head').css('display', 'none');

            $('.content.title').css('width', '100%').css('display', 'inline');
            $('.content.runtime').css('width', '100%').css('text-align', 'left').css('padding-right', '0px');
            $('.content.year').css('width', '100%').css('text-align', 'left').css('padding-right', '0px');
            $('.content.m_rating').css('width', '100%').css('text-align', 'left').css('padding-right', '0px');
            $('.content.our_rating').css('width', '100%').css('text-align', 'left').css('padding-right', '0px');
        }

        function view_grid()
        {
            $('div#movie_info_box').css('display', 'block');
            $('.img_wrapper').css('display', 'block').css('height', '200px').css('width', '145px');
            $('span.header').css('display', 'inline');
            $('.movie_item').css('margin-bottom', '0px');
            $('div.content').css('display', 'none');
            $('div.movie_item').css('width', '145px').css('height', '199px').css('padding', '0px').css('float', 'left');
            $('div#movie_head').css('display', 'none');
        }
    }
});