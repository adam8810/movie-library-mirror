$(function() {
    var typingTimer;
    var doneTypingInterval = 1000;

    $('#search_list').keyup(function() {
        typingTimer = setTimeout(doneTyping, doneTypingInterval);
    });

    //on keydown, clear the countdown 
    $('#search_list').keydown(function() {
        clearTimeout(typingTimer);
    });

    function doneTyping() {
        $search = $('#search_list').val();
        $('ul.thumbnails a.thumbnail').each(function() {
            if (this.title.toLowerCase().indexOf($search) >= 0)
                $(this).parent().show({
                    duration: 200,
                    easing: 'linear'
                });
            else
                $(this).parent().hide(200);
        });
    }
});