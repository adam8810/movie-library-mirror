#Movie Lister Web App
##Version 0.5.3

Written by [Adam Booth](http://ambooth.com "Check out his site")

## Resources
- [FuelPHP Framework](http://fuelphp.com/)
- [Bootstrap Framework](http://getbootstrap.com)
- [themoviedb.org](http://themoviedb.org/)
- [TMDb PHP API](https://github.com/glamorous/TMDb-PHP-API)

### User support
- Socal Support
	- Follow friends 
	- View watched and rated movies


### Movie Library
- Add/Remove movies from Viewed / Wishlist lists

### Netflix Integration
- Netflix Interaction
- Check if movie is available for Instant Viewing
- Add to queue

### RedBox Integration
- Check if movie available in kiosk
- Check if movie is available for Instant Viewing
