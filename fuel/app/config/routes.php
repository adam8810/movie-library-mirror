<?php
return array(
	'_root_'  => 'app/login',  // The default route,
        'logout' => 'app/logout',
        'signup' => 'app/signup',
        'list/(:num)' => 'list/index/$1',  
        'search' => '/app/search',
        'user/profile' => '/user/profile',
        'user/(:any)' => '/user/profile/$1'
);