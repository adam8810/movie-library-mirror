<?php
/**
 * The development database settings. REMOVE example from db.example.php
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'mysql:host=server;dbname=database_name',
			'username'   => 'username',
			'password'   => 'password',
		),
	),
);
