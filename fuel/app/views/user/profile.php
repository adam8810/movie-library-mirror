<div class="container-fluid">
    <div class="row-fluid">
        <div class="well offset2 span3">
            <img src="<?php echo $grav_url; ?>"/>
        </div>
        <div class="span4 well">
            <h4>My Lists</h4>
            <div class="input-append">
                <input class="span10" type="text" placeholder="Add List"/>
                <button class="btn btn-primary">Add</button>
            </div>
            <table class="table table-striped table-hover table-condensed">
                <thead>
                    <tr>
                        <th>List Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($owned_lists as $l): ?>
                    <tr>
                        <td class="span10"><a href="/list/<?php echo $l->id; ?>"><?php echo $l->name ?></a></td>
                        <td class="span1"><button class="btn btn-mini"><i class="icon-share-alt"></i></button></td>
                        <td class="span1"><button class="btn btn-mini btn-danger"><i class="icon-trash icon-white"></i></button></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <h4>Shared With Me</h4>
            <table class="table">
                <thead>
                    <tr>
                        <th>List Name</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($shared_lists as $l): ?>
                    <tr>
                        <td><a href="/list/<?php echo $l->id ?>"><?php echo $l->name; ?></a></td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>