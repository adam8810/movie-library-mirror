<ul class="breadcrumb">
    <li><a href="/">Home</a> <span class="divider">/</span></li>
    <li><a href="/user/profile">Lists</a> <span class="divider">/</span></li>
    <li class="active"><?php echo $list_name; ?></li>
</ul>
<div class="row-fluid">
    <div class="span2">
        <a class='btn btn-mini' href="/user/profile"><i class="icon-chevron-left"></i> Back</a>
    </div>
    <div class="offset8 span2">
        <?php echo $search_list->field('search_list'); ?>
    </div>
</div>
<div class="row">
    <div class="span12">
        <ul class="thumbnails">
            <?php if (isset($movies)) : ?>
                <?php foreach ($movies as $index => $m) :
                    ?>
                    <li class="span2 rel">
                        <a class="thumbnail search_thumbnail thumbnail_button" data-toggle="modal" id="<?php echo $m->tmdb_id ?>" href="#movie_info" role="button" title="<?php echo $m->title; ?>">
                            <?php echo $m->poster == null ? '<span class="abs movie_title">' . $m->title . '</span>' : '' ?>
                            <?php if ($m->poster != null): ?>
                                <img src="http://cf2.imgobject.com/t/p/w185<?php echo $m->poster ?>" width="100%"/>
                            <?php else: ?>
                                <img src="/assets/img/missing_<?php echo rand(1, 5); ?>.png" width="100%"/>
                            <?php endif; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</div>
<?php echo isset($modal) ? $modal : ''; ?>