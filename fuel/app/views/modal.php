<!-- Modal -->
<div class="modal hide" id="movie_info"  data-keyboard="true"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-keyboard="true">
    <div id="modal_img_container" class="rel">
        <div id="rating_badge" class="abs abs-top abs-right" >
            <span style="display:none" class="" id="modal_rating"></span>
        </div>
        <div id="modal_backdrop"></div>
        <div id="modal_bottom_info" class="abs abs-bottom">
            <h3 id=""><span id="modal_title"></span><span title="" id="modal_release_date"></span></h3>
            <p id="modal_tagline"></p>
        </div>
    </div>
    <div class="modal-body">
        <h4 id="modal_loading" style="display:none">Loading...</h4>
        <div class="progress progress-striped active" id="modal_progress">
            <div class="bar" style="width: 100%;"></div>
        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <ul class="nav nav-tabs">
            <li class="active"><a href="#description" data-toggle="tab">Main</a></li>
            <li><a href="#cast" data-toggle="tab">Cast</a></li>
            <li><a href="#trailers" data-toggle="tab">Trailers</a></li>
            <!--<li><a href="#similar" data-toggle="tab">Similar</a></li>-->
        </ul>
        <div id="modal_content" class="tab-content">
            <!-- Main -->
            <div class="tab-pane active rel" id="description">
                <span class="" id="modal_runtime"></span>
                <div class="abs abs-top abs-right">
                    <table>
                        <tr>
                            <td><i id="1star" class="icon-star rating_star" style="display:none"></i></td>
                            <td><i id="2star" class="icon-star rating_star" style="display:none"></i></td>
                            <td><i id="3star" class="icon-star rating_star" style="display:none"></i></td>
                            <td><i id="4star" class="icon-star rating_star" style="display:none"></i></td>
                            <td><i id="5star" class="icon-star rating_star" style="display:none"></i></td>
                        </tr>
                    </table>
                </div>
                <p id="modal_desc"></p>
                <div class="row-fluid">
                    <div class="span12">
                        <input id="add_to_list" type="hidden" />
                    </div>
                </div>
            </div>
            <!-- Cast -->
            <div class="tab-pane" id="cast">
                <ul class="thumbnails" id="modal_cast"></ul>
            </div>
            <!-- Trailers -->
            <div class="tab-pane" id="trailers">
                <ul id="modal_trailers" class="thumbnails"></ul>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <!--<p class="abs abs-bottom"><span  id="modal_runtime"></span></p>-->
<!--            <span id="modal_rating"></span></p>-->
        <!--        <button class="btn btn-primary" >Add to Watch List</button>-->
        <button class="btn btn-block btn-large" data-dismiss="modal" aria-hidden="true">Close</button>
    </div>
</div>