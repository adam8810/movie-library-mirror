<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Movie Library BETA - <?php echo $title; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php echo Asset::css(array('bootstrap.min.css', 'select2.css', 'style.css', 'bootstrap-responsive.min.css')); ?>
    </head>
    <body>
        <!-- nav -->
        <?php echo $navbar; ?>
        <!-- end nav -->
        <div class="container">
            <h4 class="pull-right"><?php echo isset($count)? $count.' in list': '' ?></h4>
            <h1 id="header"><?php echo $title; ?></h1>
            
            <?php echo $body; ?>
        </div>
        <?php echo Asset::js(array('jquery.js','uservoice.js', 'bootstrap.min.js', 'select2.min.js', 'logic.js')) ?>
        <?php echo isset($js) ? Asset::js($js) : '';?>
    </body>
</html>