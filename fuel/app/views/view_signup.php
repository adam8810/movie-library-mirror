<div class="container-fluid">
    <div class="row-fluid">
        <div class="offset2 span3 well">

            <h2>Welcome!</h2>

            <p>
                Create a new one! Signing up is easy! Just fill out the following information and you will be well on your way to an easier way to keep track of your movie library.</p>
            <p>Once you create your account an email will be sent to the email account you provided to verify your account. 
            </p>
        </div>
        <div class="span5">
            <form method="post" action="signup" class="well form-horizontal">
                <h2>Sign Up</h2>
                <fieldset>

                    <div class="control-group">
                        <label class="control-label" for="full_name">Full Name:</label>
                        <div class="controls">
                            <input name="fullname" type="text" id="full_name" class="input-large">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="username">Username:</label>
                        <div class="controls">
                            <input name="username" type="text" id="username" class="input-large">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password1">Password:</label>
                        <div class="controls">
                            <input name="password1" type="password" id="password1" class="input-large">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="password2">Confirm Password:</label>
                        <div class="controls">
                            <input name="password2" type="password" id="password2" class="input-large">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email1">Email:</label>
                        <div class="controls">
                            <input name="email1" type="text" id="email1" class="input-large">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email2">Confirm Email:</label>
                        <div class="controls">
                            <div class="input-prepend"><input name="email2" type="text" id="email2" class="input-large">
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit"><i class="icon-ok icon-white"></i> Create Account</button>
                            <button class="btn"><i class="icon-remove"></i> Cancel</button>
                        </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>