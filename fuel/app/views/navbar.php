<div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar collapsed" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="brand" href="/">Movie Library <span class="label">BETA</span> <?php echo $connected ? '' : '<span class="label">Offline</span>' ?></a>
            <?php if ($logged_in): ?>
                <div class="nav-collapse navbar-responsive-collapse collapse" style="height: 0px;">
                    <form class="navbar-search pull-left" action="/search">
                        <?php echo $form->field('q') ?>
                    </form> 
                    <ul class="nav">
                        <li class="divider-vertical"></li>
                        <li class="<?php echo $url == 'search' ? 'active' : '' ?>"><a href="/search">Search</a></li>
                        <li class="<?php echo Uri::segment(1) == 'list' ? 'active' : '' ?> dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">My Lists<b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li class="header hidden-phone hidden-tablet">My Lists</li>
                                <?php foreach ($owned_lists as $l): ?>
                                    <li class="<?php echo Uri::segment(2) == $l->id ? 'active' : '' ?>"><a href="/list/<?php echo $l->id ?>"><?php echo $l->name; ?></a></li>
                                <?php endforeach; ?>
                                <li class="header hidden-phone hidden-tablet">Shared With Me</li>
                                <?php foreach ($shared_lists as $l): ?>
                                    <li><a href="/list/<?php echo $l->id ?>"><?php echo $l->name; ?></a></li>
                                <?php endforeach; ?>
                                <li class="divider"></li>
                                <li><a href="/user/profile">View All</a></li>
                            </ul>
                        </li>
			<li><a href="javascript:void(0)" data-uv-lightbox="classic_widget" data-uv-mode="full" data-uv-primary-color="#66b5d1" data-uv-link-color="#007dbf" data-uv-default-mode="feedback" data-uv-forum-id="154292">Feedback &amp; Support</a></li>
                    </ul>
                    <ul class="nav pull-right">
                        <li class="divider-vertical"></li>
                        <li class="dropdown">
                            <a id="user_id" value="<?php echo $user_id; ?>" href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $user_image; ?> <?php echo $username; ?> <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="components.html#">Action</a></li>
                                <li><a href="components.html#">Another action</a></li>
                                <li><a href="components.html#">Something else here</a></li>
                                <li class="divider"></li>
                                <li><a href="/logout">Logout</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>