<div class="row">
    <div class="span12">
        <ul class="thumbnails">
            <?php if (@$movies) : ?>
                <?php foreach ($movies as $index => $m) : ?>
                    <li class="span2 rel">
                        <a role="button" data-toggle="modal" id="<?php echo $m->id ?>" href="#movie_info" class="thumbnail search_thumbnail thumbnail_button">
                            <?php echo is_null($m->poster) ? '<span class="abs movie_title">'.$m->title . '</span>' : ''?>
                            <img src="<?php echo !is_null($m->poster) ? $m->poster : "/assets/img/missing_" . (($index % 5) + 1) . ".png"; ?>" alt="<?php echo $m->title ?>" width="100%"/>
                        </a>
                    </li>
                <?php endforeach; ?>

            <?php else: ?>
                <div style="text-align:center" class="well offset4 span4">
                    <h4>Working Offline</h4>
                    <p>No Cache</p>
                </div>
            <?php endif; ?>
        </ul>
    </div>
</div>
<?php echo isset($modal) ? $modal: ''; ?>