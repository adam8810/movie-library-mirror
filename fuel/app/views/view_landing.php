<div class="container">
    <div class="row">
        <div class="offset3 span5">
            <div class="well">
                <form class="form-horizontal" method="post" action="/">
                    <h2>Sign In</h2>
                    <!--<fieldset>-->
                        <div class="control-group">
                            <label class="control-label" for="registered_username">Username:</label>
                            <div class="controls">
                                <input class="input-large" type="text" name="registered_username">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="registered_password">Password:</label>
                            <div class="controls">
                                <input class="input-large" type="password" name="registered_password">
                            </div>
                        </div>
                        <!--<div class="form-actions">-->
                            <button type="submit" class="btn btn-large btn-primary btn-block">Sign In</button>
                        <!--</div>-->
                    <!--</fieldset>-->
                </form>
            </div>
            <form style="display:none" class="well form-horizontal" action="signup" method="post">
                <h2>Sign Up</h2>
                <fieldset>
                    <div class="control-group">
                        <label class="control-label" for="first_name">First Name:</label>
                        <div class="controls"><input type="text" id="first_name" class="input-large" size="0">

                        </div>

                    </div>
                    <div class="control-group">
                        <label class="control-label" for="last_name">Last Name:</label>
                        <div class="controls"><input type="text" id="last_name" class="input-large">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="username">Username:</label>
                        <div class="controls">
                            <input type="text" id="username" class="input-large">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email1">Email:</label>
                        <div class="controls">
                            <input type="text" id="email1" class="input-large">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="email2">Confirm Email:</label>
                        <div class="controls">
                            <div class="input-prepend"><input type="text" id="email2" class="input-large">
                            </div>
                        </div>
                        <div class="form-actions">
                            <button class="btn btn-primary" type="submit"><i class="icon-ok icon-white"></i> Create Account</button>
                            <button class="btn" type="submit"><i class="icon-remove"></i> Cancel</button>
                        </div>
                </fieldset>
            </form>
        </div>


    </div>
</div>