<?php

namespace Model;

use Model\Movie;
use Apiconfig;
use Fuel\Core\Cache;
use Model\Connection;

class Search extends \Model {

    private static $connected;

    public static function _init() {
        static::$connected = Connection::check();
    }

    static function search($query) {
        $safe_query = str_replace('&', '-amp-', $query);
        $safe_query = str_replace('*', '-str-', $safe_query);
        $safe_query = str_replace('!', '-exp-', $safe_query);
        $safe_query = str_replace('%', '-pec-', $safe_query);
        $safe_query = str_replace('^', '-car-', $safe_query);
        $safe_query = str_replace('\\', '-bs-', $safe_query);
        $safe_query = str_replace('/', '-fs-', $safe_query);
        $safe_query = str_replace('(', '-po-', $safe_query);
        $safe_query = str_replace(')', '-pc-', $safe_query);
        $safe_query = str_replace('=', '-eq-', $safe_query);
        $safe_query = str_replace('+', '-pl-', $safe_query);
        $safe_query = str_replace('.', '-pd-', $safe_query);
        $safe_query = str_replace('$', '-pd-', $safe_query);
        $safe_query = str_replace('?', '-qs-', $safe_query);
        $safe_query = str_replace('<', '-lt-', $safe_query);
        $safe_query = str_replace('>', '-gt-', $safe_query);
        $safe_query = str_replace('\'', '-sq-', $safe_query);
        $safe_query = str_replace('"', '-dq-', $safe_query);
        
        $safe_query = strtolower(str_replace(" ", '_', $safe_query));
        
        $poster_url = "http://cf2.imgobject.com/t/p/";

        $poster_size = array(
            '1' => 'w92',
            '2' => 'w154',
            '3' => 'w185',
            '4' => 'w500',
            '5' => 'original'
        );

        // Create cache of search. Expires every 12 hours
        try
        {
            $movies = Cache::get('searches.' . $safe_query, static::$connected ? true : false);
        } catch (\Fuel\Core\CacheNotFoundException $e)
        {
            if (static::$connected) {
                $movie = new \Tmdb(Apiconfig::movie_api());
                $movies = $movie->searchMovie($query, 1, false, null, 'en');
                Cache::set('searches.' . $safe_query, $movies, 3600 * 12);
            }
            else
                return false;
        }

        $movies_array = array(); // Used to return movie objects
        // Create a new movie object for each movie returned in search
        foreach ($movies['results'] as $i => $m) {
            $movie = new Movie();

            $movie->title = $m['title'];
            $movie->id = $m['id'];

            if ($m['poster_path'] != "")
                $movie->poster = $poster_url . $poster_size[3] . $m['poster_path'];
            else {
                $movie->poster = null;
            }
            array_push($movies_array, $movie);
        }

        return $movies_array;
    }

    private static function rprintr($q) {
        echo "<pre>";
        print_r($q);
        echo "</pre>";
    }
}