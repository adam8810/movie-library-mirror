<?php

namespace Model;

use Auth\Auth;

class Authentication extends \Model {

    public static function create_new_user($username, $password, $email, $group, $profile_fields = array())
    {
        try
        {
            $auth = Auth::instance();
            @$auth->create_user($username, $password, $email, $group, $profile_fields);
            return true;
        } catch (\SimpleUserUpdateException $e)
        {
            return false;
        }
    }
    
    public static function login_user($username, $password)
    {
        $auth = Auth::instance();
        $auth->login($username, $password);
    }

    /**
     * 
     * @param type $username
     * @param type $old_password
     * @param type $new_password
     * @return bool 
     */
    public static function change_password($username, $old_password, $new_password)
    {
        $auth = Auth::instance();
        return $auth->change_password($old_password, $new_password, $username);
    }
    
    private static function printer($obj, $fixed = true) {
        if($fixed)
            $style = ' style="position:fixed; bottom:0; overflow:scroll;height:700px; width: 500px"';
        else
            $style = '';
        echo "<pre $style><legend>Print</legend><br/>";print_r($obj);echo '</pre>';
    }

}