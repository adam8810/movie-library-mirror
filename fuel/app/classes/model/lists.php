<?php

namespace Model;

use Auth\Auth;
use Model\Orm\Movielist;

class Lists extends \Model {
private static $user_id;

    public static function _init() {
        $user = $user_id = Auth::instance()->get_user_id();
        static::$user_id = $user[1];
    }
    
    public static function owned_lists() {
        return Movielist::query()->where('owner',static::$user_id)->order_by('name')->get();
    }
    
    public static function shared_lists() {
        return Movielist::query()->related('users')->where('users.id', static::$user_id)->where('owner','!=', static::$user_id)->order_by('name')->get();
    }
}