<?php
namespace Model;

class Movie extends \Model {
    
    public $title   = null;
    public $runtime = null;
    public $year    = null;
    public $poster  = null; 
}