<?php

namespace Model\Orm;

class User extends \Orm\Model {

    protected static $_many_many = array(
        'movielists' => array(
            'key_from' => 'id',
            'key_through_from' => 'user_id', // column 1 from the table in between, should match a users.id
            'table_through' => 'movielists_users', // both models plural without prefix in alphabetical order
            'key_through_to' => 'movielist_id', // column 2 from the table in between, should match a movielists.id
            'model_to' => 'Model\Orm\Movielist',
            'key_to' => 'id',
            'cascade_save' => false,
            'cascade_delete' => false,
        )
    );
    protected static $_properties = array(
        'id',
        'username',
        'email',
        'profile_fields'
    );

}