<?php
namespace Model\Orm;

class Rating extends \Orm\Model {
    protected static $_properties = array(
        'id',
        'user_id',
        'tmdb_id',
        'rating'
    );
}