<?php

namespace Model\Orm;

class Movielist extends \Orm\Model {

    protected static $_many_many = array(
        'users' => array(
            'key_from' => 'id',
            'key_through_from' => 'movielist_id', // column 1 from the table in between, should match a users.id
            'table_through' => 'movielists_users', // both models plural without prefix in alphabetical order
            'key_through_to' => 'user_id', // column 2 from the table in between, should match a movielists.id
            'model_to' => 'Model\Orm\User',
            'key_to' => 'id',
            'cascade_save' => false,
            'cascade_delete' => false,
        ),
        'movies' => array(
            'key_from' => 'id',
            'key_through_from' => 'movielist_id', // column 1 from the table in between, should match a users.id
            'table_through' => 'movies_movielists', // both models plural without prefix in alphabetical order
            'key_through_to' => 'movie_id', // column 2 from the table in between, should match a movielists.id
            'model_to' => 'Model\Orm\Movie',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => true,
        )
    );
    protected static $_properties = array(
        'id',
        'name',
        'owner',
        'created_at'
    );
}