<?php

namespace Model\Orm;

class Friends extends \Model_Crud{
    protected static $_table_name = "friends";
    protected static $_properties = array(
        'id',
        'pid',
        'fid'
    );
}