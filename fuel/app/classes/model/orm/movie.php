<?php

namespace Model\Orm;

class Movie extends \Orm\Model {

    protected static $_has_one = array(
        'rating' => array(
            'key_from' => 'tmdb_id',
            'model_to' => 'Model\Orm\Rating',
            'key_to' => 'tmdb_id',
            'cascade_save' => false,
            'cascade_delete' => false,
        )
    );
    protected static $_many_many = array(
        'movielists' => array(
            'key_from' => 'id',
            'key_through_from' => 'movielist_id', // column 1 from the table in between, should match a users.id
            'table_through' => 'movies_movielists', // both models plural without prefix in alphabetical order
            'key_through_to' => 'movie_id', // column 2 from the table in between, should match a movielists.id
            'model_to' => 'Model\Orm\Movielist',
            'key_to' => 'id',
            'cascade_save' => true,
            'cascade_delete' => true,
        )
    );
    protected static $_properties = array(
        'id',
        'tmdb_id',
        'poster',
        'runtime',
        'title',
        'release_date'
    );

}