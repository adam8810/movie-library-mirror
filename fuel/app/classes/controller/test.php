<?php

use Model\Orm\Movie;

class Controller_Test extends Controller {

    public function action_relations() {
        $movie = Movie::query()->related('rating')->where('tmdb_id', 24428)->get();
        foreach ($movie as $m) {
            echo'<pre>';
            print_r($m->rating->rating);
        }
    }

    // Fix for initial movies without images or other info
    public function action_fixmovies() {
        $movies = Movie::query()->get();

        foreach ($movies as $m) {
//          
            if ($m->poster == null || $m->title == null || $m->runtime == null) {
                $tmdb = New Tmdb(ApiConfig::movie_api());
                $tmdb = $tmdb->getMovie($m->tmdb_id);

                if (strtolower(substr($tmdb['original_title'], 0, 3)) == 'the') {
                    $start = substr($tmdb['original_title'], 0, 3);
                    $title = substr($tmdb['original_title'], 4) . ', ' . $start;
                }
                else
                    $title = $tmdb['original_title'];

                $m->title = $title;
                $m->poster = $tmdb['poster_path'];
                $m->runtime = $tmdb['runtime'];
                $m->release_date = $tmdb['release_date'];
                $m->save();
            }
        }
    }
}