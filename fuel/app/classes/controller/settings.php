<?php

class Controller_Settings extends Controller_Template{
    public function action_index()
    {
        
    }
    
    public function action_profile()
    {
        $view = View::forge('settings');
        
        $this->template->title = "Settings";
        $this->template->body = $view;
    }
}