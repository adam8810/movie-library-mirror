<?php

use \Fuel\Core\Fieldset;
use \Fuel\Core\Input;
use \Model\Orm\User;
use \Model\Orm\MovieList;
use Model\Lists;

class Controller_User extends Controller_Template {

    public function before() {
        parent::before();

        if (!\Auth\Auth::check())
            Fuel\Core\Response::redirect('/');
    }

    public function action_index($username = null) {
        
    }

    public function action_profile($username = null) {
        $view = View::forge('user/profile', null, false);

        if (is_null($username))
            $username = Session::get('username');

        $user = User::query()->where('username', $username)->get_one();


        if (Input::post()) {
            $new_list = MovieList::forge();
            $new_list->name = Input::post('list_name');
            $new_list->owner = $user->id;
            $new_list->save();
        }

        $form = Fieldset::forge('profile_fields');
        $form->add($name = 'list_name', $label = "List Name", $attributes = array(
            'class' => ''
        ));

        $form->add($name = 'submit_list', $label = "", $attributes = array(
            'class' => 'btn btn-primary',
            'type' => 'submit',
            'value' => 'Create New List'
        ));

        $size = 260;
        $default = '';

        $view->grav_url = "http://www.gravatar.com/avatar/" . md5(strtolower(trim($user->email))) . "?d=" . urlencode($default) . "&s=" . $size;
        $view->username = Auth\Auth::get_screen_name();
        $view->form = $form;
        $view->owned_lists = Lists::owned_lists();
        $view->shared_lists = Lists::shared_lists();
        $this->template->title = $username . '\'s Profile';
        $this->template->body = $view;
    }

    public function action_add_movie($list_id = null) {
        $movie = new Model\Movie();

        $movie->tmd_id = 2;
        $movie->user_id = 2;
        $movie->save();
    }
}