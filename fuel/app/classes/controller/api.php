<?php

use Fuel\Core\Input;
use Fuel\Core\Cache;
use Fuel\Core\DB;
use Model\Orm\Movielist;
use Fuel\Core\File;
use Model\Connection;
use Model\Orm\Movie;

class HttpCode {

    // Success
    public static $SUCCESSFUL = 200;
    // Errors
    public static $PAGENOTFOUND = 404;
    public static $INVALIDREQUEST = 400;

}

class Controller_Api extends Controller_Rest {

    public function before() {
        parent::before();

        // Check if connected to internet
        $this->connected = Connection::check();
        $this->api_key = ApiConfig::movie_api();
    }

    public function get_movie() {
        // DEBUG
//        $user_id = 3;

        $caching = true;

        $id = Input::get('id');

        if ($id == '')
            return $this->response(static::build_response_error(), HttpCode::$INVALIDREQUEST);

        try
        {
            if (!$caching)
                throw new CacheNotFoundException;

            $this->response(Cache::get('api.movieinfo.' . $id, $this->connected ? true : false), HttpCode::$SUCCESSFUL);
        } catch (CacheNotFoundException $error)
        {
            if ($this->connected) {
                $movie = new Tmdb($this->api_key);
                $movie_list = $movie->getMovie($id, null, array('releases', 'trailers'));
                if ($id != "" && !isset($movie_list['status_code'])) {
                    $collection = $movie_list['belongs_to_collection']['name'];
                    $trailer = $movie_list['trailers']['youtube'] ? $movie_list['trailers']['youtube'][0]['source'] : null;
                    $rating = $movie_list['releases']['countries'] ? $movie_list['releases']['countries'][0]['certification'] : null;

                    // Convert Rating system (10 stars == 5)
                    $vote_average = (round($movie_list['vote_average'] / 2))? : null;

                    $response = array(
                        'title' => $movie_list['title'],
                        'overview' => $movie_list['overview'],
                        'tagline' => $movie_list['tagline'],
                        'backdrop' => $movie_list['backdrop_path'] ? $movie_list['backdrop_path'] : null,
                        'runtime' => ($movie_list['runtime'] == 0) ? null : $movie_list['runtime'],
                        'collection' => $collection,
                        'vote_average' => $vote_average ? : null,
                        'mpaa_rating' => $rating,
                        'release_date_year' => date('Y', strtotime($movie_list['release_date'])),
                        'release_date_full' => date('F jS, Y', strtotime($movie_list['release_date'])),
                        'trailer' => $trailer,
//                    'user_lists' => 
                    );

                    if ($caching)
                        Cache::set('api.movieinfo.' . $id, $response);

                    return $this->response($response, $http_code = HttpCode::$SUCCESSFUL);
                }
                else {
                    return $this->response($movie_list, $http_code = HttpCode::$INVALIDREQUEST);
                }
            } else {
                return $this->response(array('Error' => 'No Cache'));
            }
        }
    }

    public function get_movie_in_list() {
        $movie_id = Input::get('movie_id');
        $user_id = Input::get('user_id');

        $user_lists = DB::select('movielists.id', 'name')->from('rel_users_lists')->join('movielists')->on('movielists.id', '=', 'rel_users_lists.id')->execute();

        $selected_lists = DB::select('movielists.id', 'movielists.name')->from('rel_movies_lists')->join('rel_users_lists')->on('rel_movies_lists.list_id', '=', 'rel_users_lists.list_id')->join('movielists')->on('movielists.id', '=', 'rel_movies_lists.list_id')->where('rel_users_lists.user_id', $user_id)->where('movie_id', $movie_id)->execute();

        $selected_lists = $selected_lists->as_array();

        echo '<pre>';
        print_r($selected_lists);

        $return = array();

        foreach ($user_lists as $u) {
            $temp = array();
            $temp['id'] = $u['id'];
            $temp['name'] = $u['name'];
            $temp['selected'] = isset($selected_lists[$u['id']]) ? 'selected' : 'not';

//            print_r($selected_lists);
            array_push($return, $temp);
        }

        // DEBUG
        return $this->response($return);
    }

    public function get_cast($movie_id = null) {
        if ($movie_id === null)
            $movie_id = \Fuel\Core\Input::get('id');


        $movie = new Tmdb($this->api_key);
        $cast = $movie->getMovieCast($movie_id);

        return $this->response($cast);
    }

    public function get_search($query) {
        $this->response(Model_Search::search($query));
    }

    public function get_trailers($id) {
        $movie = new Tmdb($this->api_key);
        $movie = $movie->getMovieTrailers($id);
        $this->response($movie);
    }

    public function get_movies_upcoming() {
        $movie = new Tmdb($this->api_key);
        $this->response($movie->getUpcomingMovies());
    }

    public function get_similar_movies($movie_id = null) {
        if ($movie_id === null)
            $movie_id = Input::get('id');

        $movie = new Tmdb();
        $list = $movie->getSimilarMovies($movie_id);
        return $this->response($list);
    }

    public function get_add_to_list() {
        $list_id = Input::get('list_id');
        $movie_id = Input::get('movie_id');
        $user_id = Input::get('user_id');

        $movie = Movie::query()->select('id')->where('tmdb_id', $movie_id)->get_one();

        if (!empty($movie)) {
            $movielist = Movielist::find($list_id);
            $movielist->movies[$movie->id] = Movie::find($movie->id);
            $movielist->save();
        } else {
            $movie = Movie::forge();
            $movie->tmdb_id = $movie_id;
            
            $tmdb = New Tmdb($this->api_key);

            if (Connection::check()) {
                $tmdb = $tmdb->getMovie($movie_id);
                try
                {
                    $poster = isset($tmdb['poster_path']) ? $tmdb['poster_path'] : null;
                } catch (ErrorException $e)
                {
                    $poster = null;
                }
            } else {
                $images = null;
            }
            
            if(strtolower(substr($tmdb['original_title'],0,3)) == 'the') {
                $start = substr($tmdb['original_title'],0,3);
                $title = substr($tmdb['original_title'],4) . ', ' .$start;
            }
            else
                $title = $tmdb['original_title'];
            
            
            $movie->title = $title;
            $movie->poster = $poster;
            $movie->runtime = $tmdb['runtime'];
            $movie->save();
            
            $movielist = Movielist::find($list_id);
            $movielist->movies[$movie->id] = Movie::find($movie->id);
            $movielist->save();

            try
            {
                $path = 'media' . DS . 'posters' . DS;
                $file_name = $movie_id . '.jpg';
                if (File::file_info($path . $file_name)) {
                    
                }
            } catch (FileAccessException $e)
            {
                $image = Request::forge('http://cf2.imgobject.com/t/p/w185/' . $poster, 'curl')->execute();
                File::create($path, $file_name, $image);
            }
        }
    }

    public function get_remove_from_list() {
        $list_id = Input::get('list_id');
        $movie_id = Input::get('movie_id');
        $movie = Movie::query()->select('id')->where('tmdb_id', $movie_id)->get_one();

        $movielist = Movielist::find($list_id);
        unset($movielist->movies[$movie->id]);
        $movielist->save();
    }

    public function get_movie_list() {
        $response_array = array();
        $list_id = Input::get('list_id');
        $list = RelMoviesList::find('all', array(
                    'where' => array(
                        array('list_id', $list_id)
                    )
        ));
        foreach ($list as $l) {
            $temp_array = array();
            $temp_array['id'] = $l->id;
            $temp_array['movie_id'] = $l->movie_id;
            array_push($response_array, $temp_array);
        }

        $response = $this->build_response($response_array, 200);

        return $this->response($response);
    }

    public function get_user_lists() {
        $user_id = Input::get('user_id');
        $movie_id = Input::get('movie_id');

        $response = array();
        $lists_query = Movielist::query()->related('users')->where('users.id', $user_id)->get();

        $selected_query = Movielist::query()->select('name')->related('movies')->related('users')->where('users.id', $user_id)->where('movies.tmdb_id', $movie_id)->get();
        $selected = array();
        foreach ($selected_query as $l) {
            $temp = array();
            $temp['id'] = $l->id;
            $temp['text'] = $l->name;
            array_push($selected, $temp);
        }
        $lists = array();
        foreach ($lists_query as $q) {
            $temp = array();
            $temp['id'] = $q->id;
            $temp['text'] = $q->name;
            array_push($lists, $temp);
        }
        return $this->response(array('lists' => $lists, 'selected' => $selected), 200);
    }

    private static function build_response($list, $status = null) {
        $response_array = array();
        if ($status != null)
            $response_array['status'] = $status;
        $response_array['results'] = $list;
        $response_array['count'] = count($list);

        return $response_array;
    }

    private static function build_response_error($message = null) {
        $response_array = array('Error' => $message == null ? 'Invalid request' : $message);
        return $response_array;
    }
}