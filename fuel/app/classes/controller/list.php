<?php

use Model\Orm\Movielist;
use Fuel\Core\Fieldset;

class Controller_List extends Controller_Template {

    function before() {
        parent::before();

        if (!Auth::check())
            Response::redirect('/');
    }

    public function action_index($list_id = null) {
        $view = View::forge('list', null, false);

        $user_id = Auth::get_user_id();
        $movielist = Movielist::query()->related('movies', array('order_by' => array('title' => 'ASC')))->where('id', $list_id)->get_one();

        $fieldset = Fieldset::forge('list');
        $fieldset->add('search_list', '', array(
            'class' => 'pull-right span12',
            'id' => 'search_list',
            'placeholder' => 'Search in List'
        ));

        $view->search_list = $fieldset;
        $view->movies = $movielist->movies;
        $view->username = Auth::get_screen_name();
        $view->modal = View::forge('modal');
        $view->list_name = $movielist->name;
        $this->template->js = array('list.js');
        $this->template->header = $movielist->name;
        $this->template->count = count($movielist->movies);
        $this->template->body = $view;
        $this->template->title = $movielist->name;
    }
}