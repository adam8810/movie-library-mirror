<?php

use Model\Authentication;
use Auth\Auth;
use Model\Search;

class Controller_App extends Controller_Template {

    public function before() {
        parent::before();
    }

    public function action_test() {
        
    }

    public function action_index() {
        return $this->action_landing();
    }

    function action_signup() {
        $fullname = Input::Post('fullname');
        $username = Input::Post('username');
        $email1 = Input::Post('email1');
        $email2 = Input::Post('email2');
        $password1 = Input::Post('password1');
        $password2 = Input::Post('password2');

        if (\Fuel\Core\Input::post())
            if ($email1 == $email2 && $password1 == $password2) {
                if (
                        $fullname != "" &&
                        $username != "" &&
                        $email1 != "" &&
                        $password1 != ""
                ) {
                    $auth = Auth::instance();

                    $id = $auth->create_user($username, $password1, $email1, 1, array('fullname' => $fullname));

                    $auth->force_login($id);

                    Fuel\Core\Response::redirect('search');
                }
            }
        $data = array();

        $this->template->title = 'Movie Lister - Signup';
        $this->template->body = View::forge('view_signup', $data);
    }

    function action_login() {
        $view = View::forge('view_landing');
        if (Auth::get_screen_name()) {
            if (!Input::get('q'))
                Fuel\Core\Response::redirect('search');
        }

        $username = Fuel\Core\Input::post('registered_username');
        $password = Fuel\Core\Input::post('registered_password');

        if (\Fuel\Core\Input::post()) {
            $auth = Auth::instance();
            if ($auth->login($username, $password)) {
                Fuel\Core\Response::redirect('app/search');
            } else {
//                echo \Fuel\Core\Input::post('registered_username') .' not valid';
            }
        }

        echo Authentication::login_user($username, $password);


        $this->template->title = 'Movie Lister';
        $this->template->body = $view;
        
    }

    public function action_logout() {
        Auth::logout();
        Fuel\Core\Response::redirect('/');
    }

    function action_search() {
        
        $view = View::forge('search');
        $query = Input::GET('q');
        // Check to make sure user is 
        if (!Auth::check()) {
            Fuel\Core\Response::redirect('/');
        }

        $view->username = Auth::get_screen_name();
        $user_id = Auth::get_user_id();
        $view->user_id = $user_id[1];



        if ($query != "")
            $view->query = 'Search - "' . $query . '"';
        else
            $view->query = "Search";

        if ($query != "") {
            $this->template->title = "Movie " . $view->query;

            // Call static movie search function
            $view->movies = Search::search($query);
        }
        else
            $this->template->title = 'Search';

        $view->modal = View::forge('modal');
        $this->template->body = $view;
    }

    function action_add_user($username = null) {

        \Fuel\Core\DB::insert('Users')->set(array(
            'username' => Fuel\Core\Input::post('username'),
            'fullname' => Fuel\Core\Input::post('fullname'),
            'email' => Fuel\Core\Input::post('email'),
            'password' => md5(Fuel\Core\Input::post('password'))
        ))->execute();
    }

    public function action_friends() {
        $user = Auth::instance();
        $id = $user->get_user_id();
        $id = $id[1];

        $this->template->content = "";

        $friend = new \Model\Friends();
        $friend->pid = $id;
        $friend->fid = 2;
        $friend->save();

        $friend_list = \Model\Friends::query()->select('pid', $id);

        foreach ($friend_list as $f) {
            echo $f;
        }
    }
}