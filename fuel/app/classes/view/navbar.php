<?php

use Model\Connection;

class View_Navbar extends \ViewModel {

    public function view() {
        $fieldset = Fieldset::forge();
        $fieldset->add('q', '', array(
            'class' => 'search-query span2',
            'placeholder' => 'Search for Movies',
            'type' => 'text',
            'value' => Input::get('q')
        ));
        $id_array = \Auth\Auth::instance()->get_user_id();
        $id = $id_array[1];
        $this->logged_in = Auth\Auth::check();
        $this->username = Auth::get_screen_name();
        $this->user_id = $id;
        $this->url = Uri::Segment(1);
        $this->connected = Connection::check();
        $this->owned_lists = Model\Lists::owned_lists();
        $this->shared_lists = Model\Lists::shared_lists();

        $gravatar = 'http://www.gravatar.com/avatar/' . md5(strtolower(Auth::get_email())) . '?s=18&d=404';

        $this->set('user_image', '<img src="' . $gravatar . '"/>', false);
        $this->set('form', $fieldset, false);
    }
}