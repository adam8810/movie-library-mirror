# Change Log

##0.5.3
* Upgrade Uservoice, put in navbar for easy access

##0.5.2
* Add Uservoice
* Fix List search

##0.5.1
* Add Searching lists

##0.5
* Fix existing movies
* Better list organization

##0.1.9
* Upgrade TMDB API Wrapper to latest version
* Optimize API call for movie_info